<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>「掲示板システム」HOME</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<a href="signup">新規登録</a>
		</div>
		<table
			style="border--width: medium; border-style: inset; -moz-border-radius: 20%">
			<tr>
				<th>id</th>
				<th>ログインID</th>
				<th>名前</th>
				<th>支店名</th>
				<th>部署・役職名</th>
				<th>アカウント状況</th>
				<th>編集</th>
			</tr>
			<c:forEach var="user" items="${users}">
				<div class="user-list">
					<tr>
						<td><c:out value="${user.id}" /></td>
						<td><c:out value="${user.loginId}" /></td>
						<td><c:out value="${user.name}" /></td>
						<td><c:out value="${user.branchName}" /></td>
						<td><c:out value="${user.positionName}" /></td>
						<td><form method="post">
								<c:choose>
									<c:when test="${user.status==0}">
										<input name="id" value="${user.id}" type="hidden" />
										<input name="status" value="1" id="status" type="hidden" />
										<input type="submit" onClick="return confirm('アカウントを停止しますか？');"
											value="停止する" />
									</c:when>
									<c:when test="${user.status==1}">
										<input name="id" value="${user.id}" type="hidden" />
										<input name="status" value="0" id="status" type="hidden" />
										<input type="submit" onClick="return confirm('アカウントを復活しますか？');"
											value="復活する" />
									</c:when>
								</c:choose>
							</form></td>
						<td><a href="edit?id=${user.id}">ユーザー編集</a></td>
					</tr>
				</div>
			</c:forEach>
		</table>

	</div>
</body>
</html>