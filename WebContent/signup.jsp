<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
<script src="./js/jquery-3.4.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="./css/createValidationEngine.jquery.css"
	type="text/css" />
<script src="./js/jquery.validationEngine-ja.js" type="text/javascript"
	charset="utf-8"></script>
<script src="./js/jquery.validationEngine.js" type="text/javascript"
	charset="utf-8"></script>
<script src="./js/run.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#checkform").validationEngine();
	});
</script>

</head>
<body>
	<h2>
		<font size="6">新規登録入力フォーム</font>
	</h2>
	<c:if test="${ not empty errorMessages }">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<form id="checkform" action="signup" method="post">
		<div class="absolute_user">
			<fieldset>
				<legend>【ユーザー】</legend>
				<p>
					名前： <input class="validate[required,maxSize[10]]" id="name"
						type="text" name="name" value="${user.name}" /><br>(10字以下)<br>
				</p>

				<p>
					ログインID： <input
						class="validate[required,minSize[6],maxSize[20],custom[onlyLetterNumber]]"
						id="loginId" type="text" name="loginId" value="${user.loginId}" /><br>
					(6字以上20字以下の半角英数字)<br>
				</p>
			</fieldset>
		</div>
		<br>

		<div class="absolute_password">
			<fieldset>
				<legend>【パスワード】</legend>
				<p>
					パスワード： <input
						class="validate[required,minSize[6],maxSize[20],custom[onlyMarkLetterNumber]]"
						name="password" id="password" type="password" /><br>
					(6字以上20字以下の記号を含む半角英数字)<br>
				</p>

				<p>
					確認用パスワード： <input class="validate[required,equals[password]]"
						name="conPassword" id="password2" type="password" /><br>
				</p>
			</fieldset>
		</div>
		<br>

		<div class="absolute_position">
			<fieldset>
				<legend>【所属】</legend>
				<p>
					支店名：<select name="branch" id="branch" class="validate[required]">
						<option value>▼項目を選択してください</option>
						<c:forEach items="${branchList}" var="branchList">
							<c:if test="${user.branch == branchList.branch}">
								<option value="${branchList.branch}" selected>
									<c:out value="${branchList.branch}.${branchList.branchName}" />
							</c:if>
							<c:if test="${user.branch != branchList.branch}">
								<option value="${branchList.branch}">
									<c:out value="${branchList.branch}.${branchList.branchName}" />
								</option>
							</c:if>
						</c:forEach>
					</select><br><br>
				</p>

				<p>
					部署・役職名：<select name="position" id="position"
						class="validate[required]">
						<option value>▼項目を選択してください</option>
						<c:forEach items="${positionList}" var="positionList">
							<c:if test="${user.position == positionList.position}">
								<option value="${positionList.position}" selected>
									<c:out
										value="${positionList.position} . ${positionList.positionName}" />
								</option>
							</c:if>
							<c:if test="${user.position != positionList.position}">
								<option value="${positionList.position}">
									<c:out
										value="${positionList.position} . ${positionList.positionName}" />
								</option>
							</c:if>
						</c:forEach>
					</select><br>
				</p>
			</fieldset>
		</div>
		<br>

		<p>
			<input class="absolute_submit" type="submit" value="登録"
				style="width: 70px; height: 50px; font: 20pt MS ゴシック;" />
		</p>
		<p id="return">
			<font size="6"><a href="./">戻る</a></font>
		</p>
	</form>
</body>
</html>