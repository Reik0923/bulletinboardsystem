package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {

	public List<Position> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM positions";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Position> positionList = getPosition(rs);
			return positionList;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Position> getPosition(ResultSet rs) throws SQLException {

		List<Position> positionList = new ArrayList<Position>();
		try {
			while (rs.next()) {
				int position = rs.getInt("position");
				String positionName = rs.getString("positionName");
				Position selectedPosition = new Position();
				selectedPosition.setPosition(position);
				selectedPosition.setPositionName(positionName);
				positionList.add(selectedPosition);
			}
			return positionList;
		} finally {
			close(rs);
		}
	}
}

