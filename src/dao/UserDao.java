package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;


public class UserDao {

	public int getLoginId(Connection connection, String loginId) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT COUNT(*) as loginIdCount FROM users WHERE loginId = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getInt("loginIdCount");
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public int getIdLoginId(Connection connection, String id, String loginId) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT COUNT(*) as idCount FROM users WHERE loginId = ? AND NOT id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, id);

			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getInt("idCount");
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("loginId");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch");
			sql.append(", position");
			sql.append(", status");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getPosition());
			ps.setInt(6,  user.getStatus());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.loginId as loginId, ");
			sql.append("users.name as name, ");
			sql.append("branches.branchName as branchName, ");
			sql.append("positions.positionName as positionName, ");
			sql.append("users.status as status, ");
			sql.append("users.created_at as created_at ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch = branches.branch ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position = positions.position ");
			sql.append("ORDER BY users.id ASC ");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("loginId");
				String name = rs.getString("name");
				String branchName = rs.getString("branchName");
				String positionName = rs.getString("positionName");
				int status = rs.getInt("status");
				Timestamp created_at = rs.getTimestamp("created_at");

				User selectedUser = new User();
				selectedUser.setId(id);
				selectedUser.setLoginId(loginId);
				selectedUser.setName(name);
				selectedUser.setBranchName(branchName);
				selectedUser.setPositionName(positionName);
				selectedUser.setStatus(status);
				selectedUser.setCreated_at(created_at);
				ret.add(selectedUser);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public User getUser(Connection connection, String id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			return toUser(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private User toUser(ResultSet rs) throws SQLException {
		try {
			User selectedUser = new User();
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("loginId");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				int position = rs.getInt("position");
				int status = rs.getInt("status");

				selectedUser.setId(id);
				selectedUser.setLoginId(loginId);
				selectedUser.setPassword(password);
				selectedUser.setName(name);
				selectedUser.setBranch(branch);
				selectedUser.setPosition(position);
				selectedUser.setStatus(status);
			}
			return selectedUser;
		} finally {
			close(rs);
		}
	}

	public void updateExceptPassword(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  loginId = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", position = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getPosition());
			ps.setInt(5, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  loginId = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", position = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getPosition());
			ps.setInt(6, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}

