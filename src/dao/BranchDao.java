package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches";

			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Branch> branchList = getBranch(rs);
			return branchList;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> getBranch(ResultSet rs) throws SQLException {

		List<Branch> branchList = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				int branch = rs.getInt("branch");
				String branchName = rs.getString("branchName");
				Branch selectedBranch = new Branch();
				selectedBranch.setBranch(branch);
				selectedBranch.setBranchName(branchName);
				branchList.add(selectedBranch);
			}
			return branchList;
		} finally {
			close(rs);
		}
	}
}
