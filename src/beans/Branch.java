package beans;

import java.io.Serializable;

public class Branch implements Serializable {
	private static final long serialVersionUID = 1L;

	private int branch;
	private String branchName;

	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
}
