package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.StatusDao;

public class StatusService {

	public void updateStatus(User user) {

		Connection connection = null;
		try {
			connection = getConnection();
			StatusDao statusDao = new StatusDao();
			statusDao.updateStatus(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
