package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	public int getLoginId(String loginId) {
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			int loginIdCount = userDao.getLoginId(connection, loginId);
			commit(connection);
			return loginIdCount;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public int getIdLoginId(String id, String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			int idCount = userDao.getIdLoginId(connection, id, loginId);
			commit(connection);
			return idCount;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
			UserDao userDao = new UserDao();
			userDao.insert(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<User> select() {
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			List<User> userList = userDao.select(connection);
			commit(connection);
			return userList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User getUser(String id) {

		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, id);
			commit(connection);
			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User user) {

		Connection connection = null;
		UserDao userDao = new UserDao();
		try {
			connection = getConnection();
			if (StringUtils.isEmpty(user.getPassword()) == true) {
				userDao.updateExceptPassword(connection, user);
			} else {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
				userDao.update(connection, user);
			}
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}