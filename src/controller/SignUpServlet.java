package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;


@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<Branch> branchList = new BranchService().select();
		List<Position> positionList = new PositionService().select();
		request.setAttribute("branchList", branchList);
		request.setAttribute("positionList", positionList);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		List<Branch> branchList = new BranchService().select();
		List<Position> positionList = new PositionService().select();

		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setConPassword(request.getParameter("conPassword"));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setPosition(Integer.parseInt(request.getParameter("position")));

			new UserService().register(user);
			response.sendRedirect("./");
		} else {
			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setPosition(Integer.parseInt(request.getParameter("position")));

			request.setAttribute("user", user);
			request.setAttribute("branchList", branchList);
			request.setAttribute("positionList", positionList);
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		UserService userService = new UserService();
		int loginIdCount = userService.getLoginId(loginId);

		if (loginIdCount != 0) {
			messages.add("このログインIDは既に使用されています");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
