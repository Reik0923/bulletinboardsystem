package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		User user = new UserService().getUser(id);
		List<Branch> branchList = new BranchService().select();
		List<Position> positionList = new PositionService().select();
		request.setAttribute("user", user);
		request.setAttribute("branchList", branchList);
		request.setAttribute("positionList", positionList);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = getEditUser(request);
		List<Branch> branchList = new BranchService().select();
		List<Position> positionList = new PositionService().select();

		if (isValid(request, messages) == true) {
			new UserService().update(user);
			session.setAttribute("user", user);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("user", user);
			request.setAttribute("branchList", branchList);
			request.setAttribute("positionList", positionList);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setPosition(Integer.parseInt(request.getParameter("position")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginId");
		//String password = request.getParameter("password");
		//String conPassword = request.getParameter("conPassword");
		//String name = request.getParameter("name");
		String id = request.getParameter("id");

		int idCount = new UserService().getIdLoginId(id, loginId);
/*
		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("^[0-9a-zA-Z]{6,20}$")) {
			messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
		} else */ if (idCount != 0 ) {
			messages.add("このログインIDは既に使用されています");
		}
		/*if (StringUtils.isEmpty(password) != true) {
			if (!password.matches("^[0-9a-zA-z-_@+*;:#$%!&]{6,20}$")) {
				messages.add("パスワードは記号を含む全ての半角文字、かつ6文字以上20文字以下、また変更しない場合は空欄で入力してください");
			}
		}
		if (!conPassword.equals(password)) {
			messages.add("パスワードが一致していません");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前は10文字以内で入力してください");
		}*/
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}

